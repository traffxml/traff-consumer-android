[![Release](https://jitpack.io/v/org.traffxml/traff-consumer-android.svg)](https://jitpack.io/#org.traffxml/traff-consumer-android)

Javadoc for `master` is at https://traffxml.gitlab.io/traff-consumer-android/javadoc/master/.

Javadoc for `dev` is at https://traffxml.gitlab.io/traff-consumer-android/javadoc/dev/.
